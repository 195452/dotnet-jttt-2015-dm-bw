﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace WindowsFormsApplication2
{
    class Sprawdzacz
    {
        public string sprawdzaj(string url, string zawiera)
        {
            WebClient klientWeb = new WebClient();
            klientWeb.DownloadData(url);
            Stream dane = klientWeb.OpenRead(url);
            StreamReader czytak = new StreamReader(dane);
            string kodHtml = czytak.ReadToEnd();

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(kodHtml);

            var nodes = doc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                if (node.GetAttributeValue("alt", "").IndexOf(zawiera) != -1) 
                    return node.GetAttributeValue("src","");
            }

            return "0";
        }
    }
}
