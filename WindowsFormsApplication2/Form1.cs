﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.IO.File.AppendAllText("jttt.log", DateTime.Now.ToString("M/d/yyyy hh:mm:ss")+" Szukanie obrazka..." + "\n");

            Sprawdzacz sprawdzacz = new Sprawdzacz();
            string url = sprawdzacz.sprawdzaj(textUrl.Text, textZawiera.Text);
            if (url == "0")
            {
                MessageBox.Show("Nie znaleziono takiego obrazka");
                System.IO.File.AppendAllText("jttt.log", DateTime.Now.ToString("M/d/yyyy hh:mm:ss") + " Nie znaleziono pasującego obrazka" + "\n");
            }
            else
            {
                System.IO.File.AppendAllText("jttt.log", DateTime.Now.ToString("M/d/yyyy hh:mm:ss") + " Znaleziono obrazek, pobieranie..." + "\n");
                Pobieracz pobieracz = new Pobieracz();
                string nazwa_pliku = pobieracz.pobieraj(url);
                if (nazwa_pliku == "0")
                {
                    MessageBox.Show("Błąd pobierania pliku");
                    System.IO.File.AppendAllText("jttt.log", DateTime.Now.ToString("M/d/yyyy hh:mm:ss") + " Błąd pobierania pliku " + "\n");
                }
                else
                    System.IO.File.AppendAllText("jttt.log", DateTime.Now.ToString("M/d/yyyy hh:mm:ss") + " Pobrano plik obrazka" + "\n");

                System.IO.File.AppendAllText("jttt.log", DateTime.Now.ToString("M/d/yyyy hh:mm:ss") + " Wysyłanie obrazka na podany adres e-mail..." + "\n");
                Wysylacz wysylacz = new Wysylacz();
                if (wysylacz.wysylaj(textMail.Text, nazwa_pliku) != -1)
                {
                    MessageBox.Show("Wysłano wiadomość");
                    System.IO.File.AppendAllText("jttt.log", DateTime.Now.ToString("M/d/yyyy hh:mm:ss") + " Wysyłanie zakończone powodzeniem" + "\n");
                }
                else
                {
                    MessageBox.Show("Błąd wysyłania wiadomości e-mail");
                    System.IO.File.AppendAllText("jttt.log", DateTime.Now.ToString("M/d/yyyy hh:mm:ss") + " Błąd podczas wysyłania wiadomości" + "\n");
                }

            }

        }


        private void buttonZakoncz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
