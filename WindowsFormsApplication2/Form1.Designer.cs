﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda wsparcia projektanta - nie należy modyfikować
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.textZawiera = new System.Windows.Forms.TextBox();
            this.textUrl = new System.Windows.Forms.TextBox();
            this.textMail = new System.Windows.Forms.TextBox();
            this.buttonDzialaj = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonZakoncz = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textZawiera
            // 
            this.textZawiera.Location = new System.Drawing.Point(15, 135);
            this.textZawiera.Name = "textZawiera";
            this.textZawiera.Size = new System.Drawing.Size(162, 20);
            this.textZawiera.TabIndex = 0;
            this.textZawiera.Text = "tym";
            // 
            // textUrl
            // 
            this.textUrl.Location = new System.Drawing.Point(15, 96);
            this.textUrl.Name = "textUrl";
            this.textUrl.Size = new System.Drawing.Size(162, 20);
            this.textUrl.TabIndex = 1;
            this.textUrl.Text = "http://demotywatory.pl/";
            // 
            // textMail
            // 
            this.textMail.Location = new System.Drawing.Point(15, 191);
            this.textMail.Name = "textMail";
            this.textMail.Size = new System.Drawing.Size(162, 20);
            this.textMail.TabIndex = 2;
            // 
            // buttonDzialaj
            // 
            this.buttonDzialaj.Location = new System.Drawing.Point(15, 224);
            this.buttonDzialaj.Name = "buttonDzialaj";
            this.buttonDzialaj.Size = new System.Drawing.Size(77, 43);
            this.buttonDzialaj.TabIndex = 3;
            this.buttonDzialaj.Text = "Działaj";
            this.buttonDzialaj.UseVisualStyleBackColor = true;
            this.buttonDzialaj.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(12, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Jeżeli obrazek na stronie:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Zawiera w opisie słowo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Wyślij go na adres e-mail:";
            // 
            // buttonZakoncz
            // 
            this.buttonZakoncz.Location = new System.Drawing.Point(100, 224);
            this.buttonZakoncz.Name = "buttonZakoncz";
            this.buttonZakoncz.Size = new System.Drawing.Size(77, 43);
            this.buttonZakoncz.TabIndex = 7;
            this.buttonZakoncz.Text = "Zakończ";
            this.buttonZakoncz.UseVisualStyleBackColor = true;
            this.buttonZakoncz.Click += new System.EventHandler(this.buttonZakoncz_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(30, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 37);
            this.label4.TabIndex = 8;
            this.label4.Text = "Jttt v1.0";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(193, 281);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonZakoncz);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonDzialaj);
            this.Controls.Add(this.textMail);
            this.Controls.Add(this.textUrl);
            this.Controls.Add(this.textZawiera);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textZawiera;
        private System.Windows.Forms.TextBox textUrl;
        private System.Windows.Forms.TextBox textMail;
        private System.Windows.Forms.Button buttonDzialaj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonZakoncz;
        private System.Windows.Forms.Label label4;

    }
}

