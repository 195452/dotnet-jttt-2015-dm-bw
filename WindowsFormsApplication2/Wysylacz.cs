﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

namespace WindowsFormsApplication2
{
    class Wysylacz
    {
        public int wysylaj(string adresEmail, string plik_zalacznika)
        {
            SmtpClient klientSmtp = new SmtpClient();

            MailAddress odKogo = new MailAddress("andrzej.dotnet@o2.pl",
               "Andrzej Dotnet",
            System.Text.Encoding.UTF8);
            MailAddress doKogo = new MailAddress(adresEmail);
            MailMessage wiadomosc = new MailMessage(odKogo, doKogo);
            wiadomosc.Body = "Siemanko, jak tam Ci mija dzień?";
            Attachment zalacznik = new Attachment(plik_zalacznika);
            wiadomosc.Attachments.Add(zalacznik);
            wiadomosc.BodyEncoding = System.Text.Encoding.UTF8;
            wiadomosc.Subject = "Wysyłam Ci śmieszny obrazek";
            wiadomosc.SubjectEncoding = System.Text.Encoding.UTF8;

            klientSmtp.Host = "poczta.o2.pl";
            klientSmtp.Port = 587;
            klientSmtp.Credentials = new NetworkCredential("andrzej.dotnet@o2.pl", "demotywatory");
            klientSmtp.Timeout = 5;

            try
            {
                klientSmtp.Send(wiadomosc);
            }
            catch (Exception ex)
            {
                wiadomosc.Dispose();
                return -1;
            }

            wiadomosc.Dispose();

            return 0;
        }

    }
}
