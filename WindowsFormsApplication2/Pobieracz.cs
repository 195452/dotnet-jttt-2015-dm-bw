﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    class Pobieracz
    {
        public string pobieraj(string url)
        {
            WebClient klient = new WebClient();
            string nazwa_pliku = "obrazek" + url.Substring(url.Length - 4);
            try
            {
                klient.DownloadFile(url, nazwa_pliku);
            }
            catch (Exception ex)
            {
                return "0";
            }
            return nazwa_pliku;
        }
    }
}
